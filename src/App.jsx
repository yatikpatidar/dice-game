import React, { useState } from 'react'

const App = () => {

  function getRandomInt() {
    return Math.floor(Math.random() * (6) + 1);
  }

  const [player1DiceScore, setPlayer1DiceScore] = useState(1);
  const [player2DiceScore, setPlayer2DiceScore] = useState(1);

  function playGame() {
    const player1Score = getRandomInt();
    const player2Score = getRandomInt();

    setPlayer1DiceScore(player1Score)
    setPlayer2DiceScore(player2Score)

  }

  return (
    < >
      <section className='text-red-500 text-[30px] '>
        <div className='bg-black mx-auto  border-2 border-red'>

          <div className='flex justify-center my-8'>{player1DiceScore === player2DiceScore ?
            ("Match Tie") : (
              player1DiceScore > player2DiceScore ?
                ("Player 1 Win") :
                ("Player 2 Win")
            )}</div>

          <div className="flex justify-evenly px-[20%]">

            <div className='flex-column align-center'>
              <h2 className='text-center'> Player 1</h2>

              <div id="img-container1" className='' >
                <img src={`diceFaces/dice-${player1DiceScore}.png`} className='h-[300px] transform transition duration-300 hover:rotate-90' alt="" />
              </div>

            </div>
            <div className='flex-column '>
              <h2 className='text-center'> Player 2</h2>

              <div id="img-container2" className=''>
                <img src={`diceFaces/dice-${player2DiceScore}.png`} className='h-[300px] transform transition duration-300 hover:rotate-90' alt="" />
              </div>

            </div>
          </div>

          <div className='flex justify-center'>

            <button onClick={() => playGame()} className='bg-red-600 text-black w-[50px] text-[16px] mb-20'>play</button>
          </div>

        </div>
      </section>
    </>
  )
}

export default App